<html>
<head>
    <script type="text/javascript" src="jquery-3.4.1.min.js"></script>
    <script>
        function ejecutaDep(){
            var t1 = $("#t1").val();
            var t2 = $("#t2").val();
            var t3 = $("#t3").val();

            if (t1 == "" || t2 == "" || t3 == ""){
                alert("Debe ingresar todos los campos");
            }else{
                $.ajax({
                    url:    'insertaDep.php',
                    type:   'GET',
                    data:   't1='          + t1 +
                            '&t2='         + t2 + 
                            '&t3='         + t3 ,
                    success: function () {
                        $("#t1").val("");
                        $("#t2").val("");
                        $("#t3").val("");
                        alert("Insertado con exito");
                    },
                    error: function (e) {
                        alert("Error");
                    }
                });

            }

        }
        function ejecutaEmp(){
            var t1 = $("#t4").val();
            var t2 = $("#t5").val();
            var t3 = $("#t6").val();
            var t4 = $("#t7").val();
            if (t1 == "" || t2 == "" || t3 == "" || t4 == ""){
                alert("Debe ingresar todos los campos");
            }else{
                $.ajax({
                    url:    'insertaEmp.php',
                    type:   'GET',
                    data:   't1='          + t1 +
                            '&t2='         + t2 + 
                            '&t3='         + t3 +
                            '&t4='         + t4,
                    success: function (data) {
                        $("#t4").val("");
                        $("#t5").val("");
                        $("#t6").val("");
                        $("#t7").val("");
                        alert(data);
                    },
                    error: function (e) {
                        alert("Error");
                    }
                });
            }
        }
    </script>
</head>
<body>
    <b>Departamentos</b>
    <table>
        <tr><td><label>nombre</label><input id="t1" /></td></tr>
        <tr><td><label>descripcion</label><input id="t2" /></td></tr>
        <tr><td><label>limite</label><input id="t3" /></td></tr>
        <tr><td><a href="javascript: ejecutaDep();">Guardar</a></td></tr>
    </table>
    <hr>
    <b>Empleados</b>
    <table>
            <tr><td><label>nombre completo</label><input id="t4" /></td></tr>
            <tr><td><label>cedula</label><input id="t5" /></td></tr>
            <tr><td><label>direccion</label><input id="t6" /></td></tr>
            <tr><td><label>departamento</label><input id="t7" /></td></tr>
            <tr><td><a href="javascript: ejecutaEmp();">Guardar</a></td></tr>
    </table>
</body>
</html>